package com.paradigma.anonymizer

import com.paradigma.anonymizer.util.HashUtils._
import com.paradigma.anonymizer.util.JsonUtils._

/**
 * Created by paradigma
 */
object FullAnonymization {

  @throws(classOf[IllegalArgumentException])
  def anonymize(input: String): String = {
    var toBeHashed: String = ""
    var result: String = ""

    if (input.nonEmpty) {
      // Parse JSON into a map of strings
      val jsonMap = toMap(input)
      if (jsonMap.nonEmpty) {
        jsonMap.keySet.foreach(k => toBeHashed += jsonMap.getOrElse(k, "") + "|")
        // Hash json fields using MurmurHash3 class
        result = hash(toBeHashed.substring(0, toBeHashed.length - 1))
      }
    } else {
      throw new IllegalArgumentException("Parameters must have content")
    }
    toJson(collection.immutable.HashMap("token" -> result))
  }

  @throws(classOf[IllegalArgumentException])
  def anonymize(input: Array[String]): Array[String] = {
    var toBeHashed: String = ""
    var result: String = ""
    if (input.length > 0) {
      input.foreach(value => toBeHashed += value + "|")
      result = hash(toBeHashed.substring(0, toBeHashed.length - 1))
    } else {
      throw new IllegalArgumentException("Parameters must have content")
    }
    Array[String](result)
  }

  @throws(classOf[IllegalArgumentException])
  def anonymize(input: Map[String, String]): Map[String, String] = {
    var toBeHashed = ""
    var token = ""
    if (input.nonEmpty) {
      input.keySet.foreach(k => toBeHashed += input.getOrElse(k, "") + "|")
      // Hash json fields using MurmurHash3 class
      token = hash(toBeHashed.substring(0, toBeHashed.length - 1))
    } else {
      throw new IllegalArgumentException("Parameters must have content")
    }
    Map[String, String](("token", token))
  }

}