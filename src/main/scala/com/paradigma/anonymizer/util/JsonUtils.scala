package com.paradigma.anonymizer.util

import org.json4s._
import org.json4s.jackson.Serialization.{read, write}


/**
 * Created by paradigma
 */
object JsonUtils {

  implicit val formats = org.json4s.jackson.Serialization.formats(NoTypeHints)

  @throws(classOf[IllegalArgumentException])
  def toMap(json: String): Map[String, String] = {
    try {
      read[Map[String, String]](json)
    } catch {
      case e: Exception => throw new IllegalArgumentException("Error reading Json")
    }
  }

  def toJson(input: Map[String, String]): String = {
    write(input)
  }
}
