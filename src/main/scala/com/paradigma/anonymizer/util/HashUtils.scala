package com.paradigma.anonymizer.util

import scala.util.hashing.MurmurHash3

/**
 * Created by paradigma
 */
object HashUtils {

  def hash(toBeHashed : String) : String ={
    MurmurHash3.arrayHash(toBeHashed.toCharArray).toHexString.toUpperCase
  }

}