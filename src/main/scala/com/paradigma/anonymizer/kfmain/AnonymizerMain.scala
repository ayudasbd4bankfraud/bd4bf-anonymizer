package com.paradigma.anonymizer.kfmain

import java.io.File
import java.util
import java.util.concurrent._
import java.util.{Collections, Properties}

import com.paradigma.anonymizer.PartialAnonymization
import com.typesafe.config.{Config, ConfigFactory}
import kafka.consumer.KafkaStream
import kafka.utils.Logging
import org.apache.kafka.clients.consumer.{ConsumerConfig, KafkaConsumer}
import org.apache.kafka.clients.producer.{KafkaProducer, ProducerRecord}
import org.apache.spark.streaming.kafka._
import org.apache.spark.SparkConf
import org.apache.spark.SparkContext
import kafka.serializer.StringDecoder
import org.apache.spark.streaming.{Milliseconds, StreamingContext}

import scala.collection.JavaConversions._
import java.util.{Date, Properties}

import org.slf4j.LoggerFactory


object AnonymizerMain extends App {
  val logger = LoggerFactory.getLogger(AnonymizerMain.getClass)


  val confFilePath = args(0)

  //println(s"ConfFile: ${confFilePath}")
  logger.info(s"ConfFile: ${confFilePath}")
  val confFile = new File(confFilePath)
  val parsedConf = ConfigFactory.parseFile(confFile)
  val config = ConfigFactory.load(parsedConf)



  //val sparkMaster = "local[2]"
  val sparkMaster = config.getString("spark_master")
  val brokers = config.getString("kafka_brokers")
  val groupId = config.getString("kafka_group_id")
  val topicConsumer = config.getString("kafka_consumer")
  val topicProducer = config.getString("kafka_producer")
  val anonymizeFields : List[String] = config.getStringList("anonymize_fields").to[List]

  val sparkConf = new SparkConf().setAppName("kafka").setMaster(sparkMaster).set("spark.driver.allowMultipleContexts", "true")
  lazy val sc = new SparkContext(sparkConf)



  // Create context with N millisecond batch interval
  val interval = (10000).toInt
  val ssc = new StreamingContext(sparkConf, Milliseconds(interval))
  // Create direct kafka stream with brokers and topics
  val topicsSet = topicConsumer.split(",").toSet
  val kafkaParams = Map[String, String]("metadata.broker.list" -> brokers)

  val messages = KafkaUtils.createDirectStream[String, String, StringDecoder, StringDecoder](
    ssc, kafkaParams, topicsSet).map(_._2)

  messages.count.map(cnt => "The number of received messages is " + cnt.toString).print


  logger.info("start run")


  messages.foreachRDD{rdd => rdd.foreach{msg=>
    //val anonymizeMsg = PartialAnonymization.anonymize(msg, List("name", "surname", "idno"))

    val t1 = System.currentTimeMillis()
    val anonymizeMsg = PartialAnonymization.anonymize(msg, anonymizeFields )

    val propsProducer = new Properties()
    propsProducer.put("bootstrap.servers", brokers)
    propsProducer.put("client.id", "ScalaProducerAnonymizer")
    propsProducer.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer")
    propsProducer.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer")


    val producer = new KafkaProducer[String, String](propsProducer)
    val t2 = System.currentTimeMillis()
    //val runtime = new Date().getTime()

    val data = new ProducerRecord[String, String](topicProducer, groupId, anonymizeMsg)

    producer.send(data)


    //System.out.println("sent data: " + data.value())
    logger.debug("sent data: " + data.value())
    logger.debug("Processed in " + (t2-t1) + " ms")
    producer.close()
  }}



  ssc.start()
  ssc.awaitTermination()


}
