package com.paradigma.anonymizer

import com.paradigma.anonymizer.util.HashUtils._
import com.paradigma.anonymizer.util.JsonUtils._

import scala.collection.mutable

/**
 * Created by paradigma
 */
object PartialAnonymization {

  @throws(classOf[IllegalArgumentException])
  def anonymize(input: String, fields: List[String]): String = {
    // Parse JSON into a map of strings
    var result: mutable.Map[String, String] = mutable.HashMap.empty[String, String]

    if (input.nonEmpty) {
      val jsonMap = toMap(input)
      if (jsonMap.nonEmpty) {
        result ++= jsonMap
        fields.foreach(key => result += key -> hash(jsonMap.getOrElse(key, "")))
      }
    } else {
      throw new IllegalArgumentException("Parameters must have content")
    }
    toJson(result.toMap)
  }

  @throws(classOf[IllegalArgumentException])
  def anonymize(header: Array[String], values: Array[String], fields: List[String]): Array[String] = {
    var result = scala.collection.mutable.ArrayBuffer.empty[String]
    if (header.nonEmpty && values.nonEmpty && fields.nonEmpty) {
      if (header.length == values.length) {
        //        (header, values).zipped.foreach((k, v) => result.addString(new StringBuilder(if (fields.contains(k)) hash(v) else v)))
        (header, values).zipped.foreach((k, v) => result += (if (fields.contains(k)) hash(v) else v))
      } else {
        throw new IllegalArgumentException("Header and values parameters must have same size")
      }
    } else {
      throw new IllegalArgumentException("Parameters must have content")
    }
    result.toArray
  }

  @throws(classOf[IllegalArgumentException])
  def anonymize(input: Map[String, String], fields: List[String]): Map[String, String] = {
    val result: collection.mutable.Map[String, String] = collection.mutable.Map[String, String]()
    if (input.nonEmpty && fields.nonEmpty) {
      input.keySet.foreach(key => result(key) = if (fields.contains(key)) hash(input(key)) else input(key))
    } else {
      throw new IllegalArgumentException("Parameters must have content")
    }
    result.toMap
  }

}