package com.paradigma.anonymizer

import org.scalatest.{FlatSpec, Matchers}

/**
 * Created by paradigma
 */
class PartialAnonymizationTest extends FlatSpec with Matchers {

  "Json - An 'anonymize' method execution with correct values " should " return a Json object containing a token" in {
    // prepare test data
    val json1 = "{\"name\" : \"Avelino\"}"
    val list1 = List[String]("name")
    val json2 = "{\"name\" : \"Avelino\", \"lastname\" : \"Collar\"}"
    val list2 = List[String]("lastname")
    val json3 = "{\"name\" : \"Avelino\", \"lastname\" : \"Collar\", \"zip\" : \"39005\", \"role\" : \"Developer\"}"
    val list3 = List[String]("zip")

    // execute
    val hash1 = PartialAnonymization.anonymize(json1, list1)
    val hash2 = PartialAnonymization.anonymize(json2, list2)
    val hash3 = PartialAnonymization.anonymize(json3, list3)

    // assert
    hash1 should be("{\"name\":\"2A159439\"}")
    hash2 should be("{\"name\":\"Avelino\",\"lastname\":\"95A2DF9E\"}")
    hash3 should be("{\"role\":\"Developer\",\"zip\":\"274D78A7\",\"name\":\"Avelino\",\"lastname\":\"Collar\"}")

  }

  "Json - An 'anonymize' method execution with incorrect values " should " return a IllegalArgumentException" in {
    // prepare test data
    val json1 = ""
    val list1 = List[String]("lastname")
    val json2 = "asdf"
    val list2 = List[String]("zip")
    val json3 = "{}"
    val list3 = List[String]("zip")

    // execute & assert
    val caught1 = evaluating {
      PartialAnonymization.anonymize(json1, list1)
    } should produce[IllegalArgumentException]
    caught1.getMessage should be("Parameters must have content")

    val caught2 = evaluating {
      PartialAnonymization.anonymize(json2, list2)
    } should produce[IllegalArgumentException]
    caught2.getMessage should be("Error reading Json")

    val emptyHash = PartialAnonymization.anonymize(json3, list3)
    emptyHash should be("{}")

  }

  "Array - An 'anonymize' method execution with correct values " should " return an Array object containing a token" in {
    // prepare test data
    val header = Array[String]("name", "lastname")
    val values = Array[String]("Avelino", "Collar")
    val fields = List[String]("name")

    // execute
    val hash1 = PartialAnonymization.anonymize(header, values, fields)

    // assert
    hash1(0) should be("2A159439")
    hash1(1) should be("Collar")

  }

  "Array - An 'anonymize' method execution with incorrect values " should " return a IllegalArgumentException" in {
    // prepare test data
    val header1 = Array[String]()
    val values1 = Array[String]()
    val fields1 = List[String]()

    val header2 = Array[String]("name", "lastname")
    val values2 = Array[String]("Avelino")
    val fields2 = List[String]("name")

    // execute & assert
    val caught1 = evaluating {
      PartialAnonymization.anonymize(header1, values1, fields1)
    } should produce[IllegalArgumentException]
    caught1.getMessage should be("Parameters must have content")

    val caught2 = evaluating {
      PartialAnonymization.anonymize(header2, values2, fields2)
    } should produce[IllegalArgumentException]
    caught2.getMessage should be("Header and values parameters must have same size")

  }

  "Map - An 'anonymize' method execution with correct values " should " return a Map object containing a token" in {
    // prepare test data
    val values = Map(("name", "Avelino"), ("lastname", "Collar"))
    val fields = List[String]("name")

    // execute
    val hash1 = PartialAnonymization.anonymize(values, fields)

    // assert
    hash1("name") should be("2A159439")
    hash1("lastname") should be("Collar")

  }

  "Map - An 'anonymize' method execution with incorrect values " should " return a IllegalArgumentException" in {
    // prepare test data
    val values = Map[String, String]()
    val fields = List[String]("name")

    // execute & assert
    val caught = evaluating {
      PartialAnonymization.anonymize(values, fields)
    } should produce[IllegalArgumentException]
    caught.getMessage should be("Parameters must have content")

  }

}