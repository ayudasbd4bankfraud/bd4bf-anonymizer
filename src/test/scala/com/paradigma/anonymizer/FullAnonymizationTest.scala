package com.paradigma.anonymizer

import org.scalatest.{FlatSpec, Matchers}

/**
 * Created by paradigma
 */
class FullAnonymizationTest extends FlatSpec with Matchers {

  "Json - An 'anonymize' method execution with correct values " should " return a Json object containing a token" in {
    // prepare test data
    val json1 = "{\"name\" : \"Avelino\"}"
    val json2 = "{\"name\" : \"Avelino\", \"lastname\" : \"Collar\"}"
    val json3 = "{\"name\" : \"Avelino\", \"lastname\" : \"Collar\", \"zip\" : \"39005\", \"role\" : \"Developer\"}"
    val json4 = "{}"

    // execute
    val hash1 = FullAnonymization.anonymize(json1)
    val hash2 = FullAnonymization.anonymize(json2)
    val hash3 = FullAnonymization.anonymize(json3)
    val hash4 = FullAnonymization.anonymize(json4)

    // assert
    hash1 should be("{\"token\":\"2A159439\"}")
    hash2 should be("{\"token\":\"3FE47B62\"}")
    hash3 should be("{\"token\":\"5DEFE4DE\"}")
    hash4 should be("{\"token\":\"\"}")

  }

  "Json - An 'anonymize' method execution with incorrect values " should " return a IllegalArgumentException" in {
    // prepare test data
    val json1 = ""
    val json2 = "asdf"

    // execute & assert
    val caught2 = evaluating {
      FullAnonymization.anonymize(json1)
    } should produce[IllegalArgumentException]
    caught2.getMessage should be("Parameters must have content")

    val caught3 = evaluating {
      FullAnonymization.anonymize(json2)
    } should produce[IllegalArgumentException]
    caught3.getMessage should be("Error reading Json")

  }

  "Array - An 'anonymize' method execution with correct values " should " return an Array object containing a token" in {
    // prepare test data
    val values1 = Array[String]("Avelino", "Collar")

    // execute
    val hash1 = FullAnonymization.anonymize(values1)

    // assert
    hash1(0) should be("3FE47B62")
  }

  "Array - An 'anonymize' method execution with incorrect values " should " return a IllegalArgumentException" in {
    // prepare test data
    val values = Array[String]()

    // execute & assert
    val caught = evaluating {
      FullAnonymization.anonymize(values)
    } should produce[IllegalArgumentException]
    caught.getMessage should be("Parameters must have content")

  }

  "Map - An 'anonymize' method execution with correct values " should " return an Map object containing a token" in {
    // prepare test data
    val values = Map[String, String](("name", "Avelino"), ("lastname", "Collar"))

    // execute
    val hash = FullAnonymization.anonymize(values)

    // assert
    hash("token") should be("3FE47B62")
  }

  "Map - An 'anonymize' method execution with incorrect values " should " return a IllegalArgumentException" in {
    // prepare test data
    val values = Map[String, String]()

    // execute & assert
    val caught = evaluating {
      FullAnonymization.anonymize(values)
    } should produce[IllegalArgumentException]
    caught.getMessage should be("Parameters must have content")

  }

}