name := "anonymizer"

organization := "com.paradigma"

version := "1.0.0"

scalaVersion in ThisBuild := "2.10.5"

libraryDependencies ++= Seq(
  "org.scala-lang" % "scala-compiler" % "2.10.5",
  "org.scala-lang" % "scala-reflect" % "2.10.5",
  "org.json4s" % "json4s-jackson_2.10" % "3.2.11",
  "org.scalatest" % "scalatest_2.10" % "2.0" % "test",
  "org.apache.kafka" % "kafka_2.10" % "0.8.2.1",
  "org.apache.spark" % "spark-core_2.10" % "1.5.2",
  "org.apache.spark" % "spark-streaming_2.10" % "1.5.2",
  "org.apache.spark" % "spark-streaming-kafka_2.10" % "1.5.2"
)

assemblyMergeStrategy in assembly := {
  case "reference.conf" => MergeStrategy.concat
  case PathList("META-INF", "MANIFEST.MF") => MergeStrategy.discard
  case m if m.toLowerCase.matches("meta-inf/services/.*") => MergeStrategy.concat
  case m if m.toLowerCase.matches("meta-inf/.*\\.sf$") => MergeStrategy.discard
  case _ => MergeStrategy.first

}
