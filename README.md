### BD4BF-Anonymizer ###

Scalable and Robust Anonymizer system

### Instructions ###

* Compilation
Compile using sbt assembly

For testing and development the jar can be executed directly
  java -jar bd4bf-anonymizer/anonymizer-assembly-1.0.0.jar bd4bf-anonymizer/anonymizer.conf

For cluster operation, it should be submited to Spark (
  spark-submit bd4bf-anonymizer/anonymizer-assembly-1.0.0.jar bd4bf-anonymizer/anonymizer.conf
  


### License ###

Apache2

### Contanct ###

info@paradigmadigital.com